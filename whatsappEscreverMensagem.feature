# language: pt

Funcionalidade: Iniciar uma nova conversa	

Como usuário do Whatsapp
Eu quero iniciar uma nova conversa
Para mandar um recado para um contato


  Cenario: Iniciar nova conversa
    Dado que eu estou com o Whatsapp aberto
    Quando eu seleciono opção "Escrever Mensagem"
    E na tela de contatos seleciono um contato
    Então janela da conversa é exibida vazia
    E digito uma mensagem 
    E seleciono a opção "Enviar"
    Então a mensagem é enviada