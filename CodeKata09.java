package DesafioQA;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import org.junit.Assert;


public class CodeKata09 {

	static String prod;
	static int qtdetotal, preço, preçodesconto;

	@Dado("^um produto \"([^\"]*)\"$")
//	@Dado("^um produto ([a-dA-D])$")
	public void um_produto(String produto) throws Throwable {
		prod = produto;
		switch (produto) {

		case "A":
			Assert.assertEquals("A", produto);
			break;
		case "B":
			Assert.assertEquals("B", produto);
			break;
		case "C":
			Assert.assertEquals("C", produto);
			break;
		case "D":
			Assert.assertEquals("D", produto);
			break;
		default:
			System.out.println("Produto não cadastrado" + " " + produto);
			break;
		}
	}

	@Quando("^eu escolher apenas (\\d+)$")
	public void eu_escolher_apenas(int qtde) throws Throwable {
		Assert.assertEquals(1, qtde);
	}

	@Então("^preço unitário é (\\d+)$")
	public void preço_unitário_é(int preçounit) throws Throwable {
		switch (preçounit) {

		case 50:
			Assert.assertEquals(preçounit, 50);
			break;
		case 30:
			Assert.assertEquals(preçounit, 30);
			break;
		case 20:
			Assert.assertEquals(preçounit, 20);
			break;
		case 15:
			Assert.assertEquals(preçounit, 15);
			break;
		default:
			System.out.println("Preço não cadastrado" + " " + preçounit);
			break;
		}
	}

	@Quando("^eu escolho (\\d+) produtos$")
	public void eu_escolho_mais_que_produtos(int qtde) throws Throwable {
		switch (qtde) {
		case 1:
			System.out.println("Quantidade escolhida é" + " " + qtde + " portanto o preço é unitário sem desconto");
			break;
		case 2:
			System.out.println("Quantidade escolhida é" + " " + qtde + " portanto o preço do item B é com desconto");
			break;
		case 3:
			if (prod == "A") {
			System.out.println("Quantidade escolhida é" + " " + qtde + " portanto o preço do item A é com desconto");
			}
			//Assert.assertEquals(prod, "A");
			System.out.println("Quantidade escolhida é" + " " + qtde + " mas o produto "+ prod +" não possui desconto");
			break;
		default:
			System.out.println("A quantidade informada não consta na tabela de descontos");
			break;
		}
	}

	@Então("^preço com desconto é de (\\d+)$")
	public void preço_com_desconto_é_de(int pdesconto) throws Throwable {
		switch (pdesconto) {
		case 130:
			System.out.println("Preço com desconto para item A"+" "+pdesconto);
			break;
		case 45:
			System.out.println("Preço com desconto para item B"+" "+pdesconto);
			break;
		default:
			System.out.println("O valor informado não consta na tabela de descontos");
			break;
		}
	}
}