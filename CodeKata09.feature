#language: pt
#Item 	Unit Price 	Special Price
#A 			50 	3 for 130
#B 			30 	2 for 45
#C 			20
#D 			15
Funcionalidade: Carrinho de compras 
							Como cliente
  						Eu quero poder comprar com desconto
  						Para economizar

  Cenário: Preço unitário de A
    Dado um produto "A"
    Quando eu escolher apenas 1
    Então preço unitário é 50

  Cenário: Preço unitário de B
    Dado um produto "B"
    Quando eu escolher apenas 1
    Então preço unitário é 30

  Cenário: Preço unitário de F
    Dado um produto "F"
    Quando eu escolher apenas 1
    Então preço unitário é 35

  Cenário: Preço com desconto para produtos A ou B
    Dado um produto "A"
    Quando eu escolho 3 produtos
    Então preço com desconto é de 130

  Cenário: Preço com desconto para produtos A ou B
    Dado um produto "B"
    Quando eu escolho 2 produtos
    Então preço com desconto é de 45

  Cenário: Preço com desconto para produtos A ou B
    Dado um produto "C"
    Quando eu escolho 3 produtos
    Então preço com desconto é de 150

  Cenário: Preço com desconto para produtos A ou B
    Dado um produto "J"
    Quando eu escolho 8 produtos
    Então preço com desconto é de 19
