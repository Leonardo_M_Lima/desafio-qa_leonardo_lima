# language: pt

Funcionalidade: Apagar histórico da conversa

Como usuário do Whatsapp
Eu quero apagar o histórico de uma conversa
Para liberar espaço utilizado


  Cenario: Apagar historico de conversa
    Dado que eu estou com o Whatsapp aberto
    Quando eu seleciono uma conversa
    E e acesso o menu "Limpar conversa"
    E confirmo
    Então janela da conversa é exibida vazia